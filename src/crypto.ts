import { hkdf as sha256HKDF } from "fast-sha256";

import { ec as Elliptic } from "elliptic";
import * as forge from "node-forge";

import { Hex, Base64, KeyPair } from "./types";
import { hexToBytes, bytesToUint8Array } from "./encoding";

const ec = new Elliptic("p256");

export const GET_RANDOM_BYTES = (n: number): Hex =>
  forge.util.bytesToHex(forge.random.getBytesSync(n));

export const EC_KEYPAIR_GENERATE = (): KeyPair => {
  const keyPair = ec.genKeyPair();

  return {
    privateKey: keyPair.getPrivate("hex"),
    publicKey: keyPair.getPublic("hex"),
    compressedPublicKey: keyPair.getPublic(true, "hex"),
  };
};

export const EC_KEYPAIR_FROM_PRIVATE_KEY = (privateKey: Hex): KeyPair => {
  const keyPair = ec.keyFromPrivate(privateKey, "hex");
  return {
    privateKey: keyPair.getPrivate("hex"),
    publicKey: keyPair.getPublic("hex"),
    compressedPublicKey: keyPair.getPublic(true, "hex"),
  };
};

export const SIGN_EC_SHA256_DER = (privateKey: Hex, data: Hex): Hex => {
  const ecPrivateKey = ec.keyFromPrivate(privateKey, "hex");
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  const signature = ecPrivateKey.sign(md.digest().toHex());
  return signature.toDER("hex");
};

export const VERIFY_EC_SHA256_DER_SIGNATURE = (
  publicKey: Hex,
  data: Hex,
  signature: Hex
): boolean => {
  const ecPublicKey = ec.keyFromPublic(publicKey, "hex");
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  return ecPublicKey.verify(md.digest().toHex(), signature);
};

export const SIGN_EC_SHA256_IEEE = (privateKey: Hex, data: Hex): Hex => {
  const ecPrivateKey = ec.keyFromPrivate(privateKey, "hex");
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  const signature = ecPrivateKey.sign(md.digest().toHex());
  const r = signature.r.toString(16, 32);
  const s = signature.s.toString(16, 32);
  return r + s;
};

export const VERIFY_EC_SHA256_IEEE_SIGNATURE = (
  publicKey: Hex,
  data: Hex,
  signature: Hex
): boolean => {
  const ecPublicKey = ec.keyFromPublic(publicKey, "hex");
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  const sig = {
    r: signature.slice(0, 64),
    s: signature.slice(64, 128),
  };
  return ecPublicKey.verify(md.digest().toHex(), sig);
};

export const ECDH = (privateKey: Hex, publicKey: Hex): Hex => {
  const ecPrivateKey = ec.keyFromPrivate(privateKey, "hex");
  const ecPublicKey = ec.keyFromPublic(publicKey, "hex");
  const sharedSecret = ecPrivateKey.derive(ecPublicKey.getPublic());

  return sharedSecret.toString("hex");
};

export const SHA256 = (data: Hex): Hex => {
  const md = forge.md.sha256.create();
  md.update(hexToBytes(data));
  return md.digest().toHex();
};

export const HMAC_SHA256 = (data: Hex, key: Hex): Hex => {
  const hmac = forge.hmac.create();
  hmac.start("sha256", hexToBytes(key));
  hmac.update(hexToBytes(data));
  return hmac.digest().toHex();
};

export const HKDF_SHA256 = async (
  data: Hex,
  length: number,
  info: Hex,
  salt: Hex
): Promise<Hex> => {
  bytesToUint8Array(hexToBytes(data));
  return forge.util
    .createBuffer(
      sha256HKDF(
        bytesToUint8Array(hexToBytes(data)),
        bytesToUint8Array(hexToBytes(salt)),
        bytesToUint8Array(hexToBytes(info)),
        length
      )
    )
    .toHex();
};

export const KDF_SHA256 = (seed: Hex, keyId: Hex): Hex => SHA256(seed + keyId);

export const ENCRYPT_AES_CTR = (data: Hex, key: Hex, iv: Hex): Hex => {
  const cipher = forge.cipher.createCipher("AES-CTR", hexToBytes(key));
  cipher.start({ iv: hexToBytes(iv) });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  cipher.finish();

  return cipher.output.toHex();
};

export const DECRYPT_AES_CTR = (data: Hex, key: Hex, iv: Hex): Hex => {
  const cipher = forge.cipher.createDecipher("AES-CTR", hexToBytes(key));
  cipher.start({ iv: hexToBytes(iv) });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  cipher.finish();

  return cipher.output.toHex();
};

export const ENCRYPT_AES_GCM = (data: Hex, key: Hex, iv: Hex) => {
  const cipher = forge.cipher.createCipher("AES-GCM", hexToBytes(key));
  cipher.start({ iv: hexToBytes(iv) });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  cipher.finish();

  const encrypted = cipher.output.toHex();
  const tag = cipher.mode.tag.toHex();

  return { encrypted, tag };
};

export const DECRYPT_AES_GCM = (
  data: Hex,
  tag: Hex,
  key: Hex,
  iv: Hex
): Hex => {
  const cipher = forge.cipher.createDecipher("AES-GCM", hexToBytes(key));
  cipher.start({
    iv: hexToBytes(iv),
    tag: forge.util.createBuffer(hexToBytes(tag)),
  });
  cipher.update(forge.util.createBuffer(hexToBytes(data)));
  cipher.finish();

  return cipher.output.toHex();
};

export const ENCRYPT_DLIES = (publicKey: Hex, data: Hex) => {
  const ephemeralKeyPair = EC_KEYPAIR_GENERATE();
  const dhKey = ECDH(ephemeralKeyPair.privateKey, publicKey);
  const encryptionKey = KDF_SHA256(dhKey, "01").slice(0, 32);
  const authKey = KDF_SHA256(dhKey, "02");
  const iv = GET_RANDOM_BYTES(16);
  const encryptedData = ENCRYPT_AES_CTR(data, encryptionKey, iv);
  const mac = HMAC_SHA256(encryptedData, authKey);

  return {
    publicKey: ephemeralKeyPair.publicKey,
    data: encryptedData,
    iv,
    mac,
  };
};

export const DECRYPT_DLIES = (
  privateKey: Hex,
  publicKey: Hex,
  data: Hex,
  iv: Hex,
  mac: Hex
): Hex => {
  const dhKey = ECDH(privateKey, publicKey);
  const encKey = KDF_SHA256(dhKey, "01").slice(0, 32);
  const authKey = KDF_SHA256(dhKey, "02");
  const macCheck = HMAC_SHA256(data, authKey);

  if (mac && mac !== macCheck) {
    throw new Error("Invalid MAC");
  }

  return DECRYPT_AES_CTR(data, encKey, iv);
};

export const DECRYPT_DLIES_WITHOUT_MAC = (
  privateKey: Hex,
  publicKey: Hex,
  data: Hex,
  iv: Hex
) => {
  const dhKey = ECDH(privateKey, publicKey);
  const encKey = KDF_SHA256(dhKey, "01").slice(0, 32);

  return DECRYPT_AES_CTR(data, encKey, iv);
};

export const GENERATE_TRACE_ID = (data: Hex, key: Hex): Base64 => {
  const hmac = forge.hmac.create();
  hmac.start("sha256", hexToBytes(key));
  hmac.update(hexToBytes(data));
  return forge.util.encode64(hmac.digest().bytes().slice(0, 16));
};
