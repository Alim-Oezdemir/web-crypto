export type Bit = 0 | 1;
export type Hex = string;
export type Z85 = string;
export type Bytes = string;
export type Base32 = string;
export type Base64 = string;
export type Base85 = string;

export type KeyPair = {
  privateKey: Hex;
  publicKey: Hex;
  compressedPublicKey: Hex;
};
